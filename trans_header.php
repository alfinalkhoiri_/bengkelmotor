<?php
	//Koneksi Database
	$server = "localhost";
	$user = "root";
	$pass = "";
	$database = "bengkelmotor";

	// $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));
	$koneksi = mysqli_connect($server, $user, $pass, $database);
		if (!$koneksi) {
			die("<script>alert('Gagal Terhubung ke Database')</script>");
	};
	
	//jika tombol simpan diklik
	if(isset($_POST['bsimpan']))
	{
		//Pengujian Apakah data akan diedit atau disimpan baru
		if($_GET['hal'] == "edittransaksi")
		{
			//Data akan di edit
			$edit = mysqli_query($koneksi, "UPDATE trans_header SET
											 	tanggal_transaksi = '$_POST[tanggal_transaksi]',
                                                id_service = '$_POST[id_service]',
												id_teknisi = '$_POST[id_teknisi]',
												id_customer = '$_POST[id_customer]'
											 WHERE id_transaksi = '$_GET[id_transaksi]'
										   ")or die(mysqli_error($koneksi));
			if($edit) //jika edit sukses
			{
				echo "<script>
						alert('Edit data suksess!');
						document.location='./index.php?hal=data_transaksi';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Edit data GAGAL!!');
						document.location='./index.php?hal=data_transaksi';
				     </script>";
			}
		}
		else
		{
			//Data akan disimpan Baru
			$simpan = mysqli_query($koneksi, "INSERT INTO trans_header SET 
                                            id_transaksi = '$_POST[id_transaksi]',
                                            tanggal_transaksi = '$_POST[tanggal_transaksi]',
                                            id_service = '$_POST[id_service]',
                                            id_teknisi = '$_POST[id_teknisi]', 
                                            id_customer = '$_POST[id_customer]'
										") or die(mysqli_error($koneksi));

            $simpan2 = mysqli_query($koneksi, "INSERT INTO transaksi_detail SET 
                                            id_transaksi = '$_POST[id_transaksi]',
                                            id_service = '$_POST[id_service]',
                                            id_sparepart = '$_POST[id_sparepart]'
                                        ") or die(mysqli_error($koneksi));
            
			if($simpan && $simpan2) //jika simpan sukses
			{
				echo "<script>
						alert('Simpan data suksess!');
						document.location='./index.php?hal=data_transaksi';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Simpan data GAGAL!!');
						document.location='./index.php?hal=data_transaksi';
				     </script>";
			}
		}

	}


	//Pengujian jika tombol Edit / Hapus di klik
	if(isset($_GET['hal']))
	{
		//Pengujian jika edit Data
		if($_GET['hal'] == "edittransaksi")
		{
            //Tampilkan Data yang akan diedit
            $tampil = mysqli_query($koneksi, "SELECT trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, 
                                    teknisi.nama_teknisi, customer.nama_cs From trans_header INNER JOIN service ON trans_header.id_service = service.id_service 
                                    INNER JOIN teknisi ON trans_header.id_teknisi = teknisi.id_teknisi INNER JOIN customer ON trans_header.id_customer = customer.id_cs 
                                    ORDER BY trans_header.id_transaksi desc");

			$data = mysqli_fetch_array($tampil);
			if($data)
			{
				//Jika data ditemukan, maka data ditampung ke dalam variabel
				// $vidtrans = $data['id_transaksi'];
                $vtanggal = $data['tanggal_transaksi'];
                $vids = $data['nama_service'];
                $vidt = $data['nama_teknisi'];
                $vidc = $data['nama_cs'];
			}
		}
		else if ($_GET['hal'] == "delete")
		{
			//Persiapan hapus data
			$hapus = mysqli_query($koneksi, "DELETE FROM trans_header WHERE id_transaksi = '$_GET[id_transaksi]' ");
			if($hapus){
				echo "<script>
						alert('Hapus Data Suksess!!');
						document.location='./index.php?hal=data_transaksi';
				     </script>";
			}
		}
	}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Customer AlvinMotor</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">
    <div class="container">     
        <h1 class="text-center">Data Transaksi Service AlvinMotor</h1>

        <!-- Awal Card Form -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <form method="post" action="">
                        <div class="form-group">
                            <label>ID Transaksi : </label>
                            <input type="text" name="id_transaksi" class="form-control" value="<?=@$vidt?>">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Transaksi : </label>
                            <input type="date" name="tanggal_transaksi" class="form-control" value="<?=@$vtanggal?>"
                             required>
                        </div>
                        <div class="form-group">
                            <label>Nama Service :</label>
                            <select class="form-control" name="id_service" value="required">
                                <option value=""><?=@$vids?></option>
                                <?php
                                    $tampil = mysqli_query($koneksi, "SELECT * FROM service");
                                    while ($data = mysqli_fetch_array($tampil)){
                                    echo "<option value=$data[id_service]> $data[nama_service] </option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Teknisi :</label>
                            <select class="form-control" name="id_teknisi" value="required">
                                <!-- <option value="<?=@$vjk?>"><?=@$vjk?></option> -->
                                <option><?=@$vidt?></option>
                                <?php
                                    $tampil = mysqli_query($koneksi, "SELECT * FROM teknisi");
                                    while ($data = mysqli_fetch_array($tampil)){
                                    echo "<option value=$data[id_teknisi]> $data[nama_teknisi] </option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Customer :</label>
                            <select class="form-control" name="id_customer" value="required">
                                <option><?=@$vidc?></option>
                                <?php
                                    $tampil = mysqli_query($koneksi, "SELECT * FROM customer");
                                    while ($data = mysqli_fetch_array($tampil)){
                                    echo "<option value=$data[id_cs]> $data[nama_cs] </option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Sparepart :</label>
                            <select class="form-control" name="id_sparepart" value="required">
                                <option><?=@$vidsp?></option>
                                <?php
                                    $tampil = mysqli_query($koneksi, "SELECT * FROM sparepart");
                                    while ($data = mysqli_fetch_array($tampil)){
                                    echo "<option value=$data[id_sparepart]> $data[nama_sparepart] </option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success" name="bsimpan" style="margin-top: 10px;">Simpan</button>
                        <button type="reset" class="btn btn-danger" name="breset" style="margin-top: 10px;">Reset</button>
                    </form>
                </div>   
        </div>
        </div>
        <!-- Akhir Card Form -->

        <!-- Awal Card Tabel -->
	    <div class="card mt-3">
            <div class="card-header bg-success text-white">
                Data Riwayat Service AlvinMotor
            </div>
	  	        <div class="card-body">
	                <div class="table-responsive">
		                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Nama Teknisi</th>
                                    <th>Nama Customer</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Nama Teknisi</th>
                                    <th>Nama Customer</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                    $no = 1;
                                    
                                    $tampil = mysqli_query($koneksi, "SELECT trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, 
                                    teknisi.nama_teknisi, customer.nama_cs From trans_header INNER JOIN service ON trans_header.id_service = service.id_service 
                                    INNER JOIN teknisi ON trans_header.id_teknisi = teknisi.id_teknisi INNER JOIN customer ON trans_header.id_customer = customer.id_cs ORDER BY trans_header.id_transaksi desc");
                                    while ($d = mysqli_fetch_array($tampil)) {
                                ?>
                                        <tr>
                                            <td><?=$no++;?></td>
                                            <td> <?php echo $d['id_transaksi'];?> </td> 
                                            <td> <?php echo $d['tanggal_transaksi'];?> </td>
                                            <td> <?php echo $d['nama_service'];?> </td>
                                            <td> <?php echo $d['nama_teknisi'];?> </td>
                                            <td> <?php echo $d['nama_cs'];?> </td>
                                            <td>
                                                <a href="./index.php?hal=edittransaksi&id_transaksi=<?=$d['id_transaksi'];?>"" class="btn btn-warning"> Edit </a>
                                                <a href="trans_header.php?hal=delete&id_transaksi=<?=$d['id_transaksi'];?>" 
                                                onclick="return confirm('Apakah yakin ingin menghapus data ini?')" class="btn btn-danger"> Hapus </a>
                                            </td>
                                        </tr>

                                    <?php
                                    }        // endwhile; //penutup perulangan while
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <!-- Akhir Card Tabel -->
    </div>
        
        <!-- Awal Card Tabel Detail-->
	    <!-- <div class="card mt-3">
            <div class="card-header bg-success text-white">
                Data Riwayat Transaksi AlvinMotor
            </div>
	  	        <div class="card-body">
	                <div class="table-responsive">
		                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Harga Service</th>
                                    <th>Nama Sparepart</th>
                                    <th>Harga Sparepart</th>
                                    <th>Total Harga</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Harga Service</th>
                                    <th>Nama Sparepart</th>
                                    <th>Harga Sparepart</th>
                                    <th>Total Harga</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                    include 'config.php';
                                    // FROM tb_siswa INNER JOIN tb_jurusan ON tb_siswa.id_jurusan = tb_jurusan.id_jurusan 
                                    // INNER JOIN tb_spp ON tb_siswa.id_spp = tb_spp.id_spp
                                    $no = 1;
                                    // SELECT * FROM karyawan, jabatan WHERE karyawan.id_jab = jabatan.id_jab
                                    $data = mysqli_query($conect, "SELECT transaksi_detail.id_tdetail, trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, service.harga_service, 
                                    sparepart.nama_sparepart, sparepart.harga_sparepart, transaksi_detail.total_harga From transaksi_detail INNER JOIN trans_header ON transaksi_detail.id_transaksi = trans_header.id_transaksi 
                                    INNER JOIN service ON transaksi_detail.id_service = service.id_service INNER JOIN sparepart ON transaksi_detail.id_sparepart = sparepart.id_sparepart ORDER BY transaksi_detail.id_tdetail desc");
                                    while ($d = mysqli_fetch_array($data)) {
                                        $d['total_harga'] = $d['harga_service'] + $d['harga_sparepart']
                                ?>
                                        <tr>
                                            <td><?=$no++;?></td>
                                            <td> <?php echo $d['id_transaksi'];?> </td> 
                                            <td> <?php echo $d['tanggal_transaksi'];?> </td>
                                            <td> <?php echo $d['nama_service'];?> </td>
                                            <td> <?php echo $d['harga_service'];?> </td>
                                            <td> <?php echo $d['nama_sparepart'];?> </td>
                                            <td> <?php echo $d['harga_sparepart'];?> </td>
                                            <td> <?php echo $d['total_harga'];?> </td>
                                        </tr>

                                    <?php
                                    }        // endwhile; //penutup perulangan while
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div> -->
        <!-- Akhir Card Tabel Detail-->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
	    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	    <!-- Core plugin JavaScript-->
	    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	    <!-- Custom scripts for all pages-->
	    <script src="js/sb-admin-2.min.js"></script>

	    <!-- Page level plugins -->
	    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
	    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	    <!-- Page level custom scripts -->
	    <script src="js/demo/datatables-demo.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>

</html>