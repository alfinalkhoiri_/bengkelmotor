<?php
	//Koneksi Database
	$server = "localhost";
	$user = "root";
	$pass = "";
	$database = "bengkelmotor";

	// $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));
	$koneksi = mysqli_connect($server, $user, $pass, $database);
		if (!$koneksi) {
			die("<script>alert('Gagal Terhubung ke Database')</script>");
	};
	
	//jika tombol simpan diklik
	if(isset($_POST['bsimpan']))
	{
		//Pengujian Apakah data akan diedit atau disimpan baru
		if($_GET['hal'] == "editservice")
		{
			//Data akan di edit
			$edit = mysqli_query($koneksi, "UPDATE service set
											 	nama_service = '$_POST[nama_service]',
												harga_service = '$_POST[harga_service]'
											 WHERE id_service = '$_GET[id]'
										   ");
			if($edit) //jika edit sukses
			{
				echo "<script>
						alert('Edit data suksess!');
						document.location='./index.php?hal=data_service';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Edit data GAGAL!!');
						document.location='./index.php?hal=data_service';
				     </script>";
			}
		}
		else
		{
			//Data akan disimpan Baru
			$simpan = mysqli_query($koneksi, "INSERT INTO service (nama_service, harga_service)
										  	VALUES ('$_POST[nama_service]',
										  		 	'$_POST[harga_service]')
										 ");
			if($simpan) //jika simpan sukses
			{
				echo "<script>
						alert('Simpan data suksess!');
						document.location='./index.php?hal=data_service';
				     </script>";
			}
			else
			{
				echo "<script>
						alert('Simpan data GAGAL!!');
						document.location='./index.php?hal=data_service';
				     </script>";
			}
		}
		
	}


	//Pengujian jika tombol Edit / Hapus di klik
	if(isset($_GET['hal']))
	{
		//Pengujian jika edit Data
		if($_GET['hal'] == "editservice")
		{
			//Tampilkan Data yang akan diedit
			$tampil = mysqli_query($koneksi, "SELECT * FROM service WHERE id_service = '$_GET[id]' ");
			$data = mysqli_fetch_array($tampil);
			if($data)
			{
				//Jika data ditemukan, maka data ditampung ke dalam variabel
				$vnama = $data['nama_service'];
				$vharga = $data['harga_service'];
			}
		}
		else if ($_GET['hal'] == "hapus")
		{
			//Persiapan hapus data
			$hapus = mysqli_query($koneksi, "DELETE FROM service WHERE id_service = '$_GET[id]' ");
			if($hapus){
				echo "<script>
						alert('Hapus Data Suksess!!');
						document.location='./index.php?hal=data_service';
				     </script>";
			}
		}
	}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Tables</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<body>
<div class="container">

	<h1 class="text-center">Data Service AlvinMotor</h1>

	<!-- Awal Card Form -->
	<div class="card shadow mb-4">
	    <div class="card-header py-3">
	        <h6 class="m-0 font-weight-bold text-primary">Data Service</h6>
	    </div>
	  <div class="card-body">
	        <div class="table-responsive">
			    <form method="post" action="">
					<div class="form-group">
						<label>Nama Service : </label>
						<input type="text" name="nama_service" class="form-control" value="<?=@$vnama?>"
						placeholder="Input Nama anda disini!" required>
					</div>
					<div class="form-group">
						<label>Harga : </label>
						<input type="text" name="harga_service" class="form-control" value="<?=@$vharga?>"
						placeholder="Input No Handphone anda disini!" required/>
					</div>

			    	<button type="submit" class="btn btn-success" name="bsimpan" style="margin-top: 10px;">Simpan</button>
			    	<button type="reset" class="btn btn-danger" name="breset" style="margin-top: 10px;">Reset</button>
		    	</form>
		    </div>	
	  </div>
	</div>
	<!-- Akhir Card Form -->

	<!-- Awal Card Tabel -->
	<div class="card mt-3">
	  <div class="card-header bg-success text-white">
	    Data Service AlvinMotor
	  </div>
	  <div class="card-body">
        <div class="table-responsive">
	        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	            <thead>
			    	<tr>
			    		<th>No.</th>
			    		<th>ID Service</th>
	                    <th>Nama Service</th>
	                    <th>Harga</th>
	                    <th>Action</th>
			    	</tr>
		    	</thead>
	            <tfoot>
	                <tr>
	                	<th>No.</th>
	                    <th>ID Service</th>
	                    <th>Nama Service</th>
	                    <th>Harga</th>
	                    <th>Action</th>
	                </tr>
	            </tfoot>
	            <tbody>
			    	<?php
			    		$no = 1;
			    		$tampil = mysqli_query($koneksi, "SELECT * from service order by id_service desc");
			    		while($data = mysqli_fetch_array($tampil)) :

			    	?>
			    	<tr>
			    		<td><?=$no++;?></td>
			    		<td><?=$data['id_service']?></td>
			    		<td><?=$data['nama_service']?></td>
						<td><?=$data['harga_service']?></td>
			    		<td>
			    			<a href="./index.php?hal=editservice&id=<?=$data['id_service']?>" class="btn btn-warning"> Edit </a>
			    			<a href="service.php?hal=hapus&id=<?=$data['id_service']?>" 
			    			   onclick="return confirm('Apakah yakin ingin menghapus data ini?')" class="btn btn-danger"> Hapus </a>
			    		</td>
			    	</tr>
		    		<?php endwhile; //penutup perulangan while ?>
		    	</tbody>
		    </table>
	  </div>
	</div>
	<!-- Akhir Card Tabel -->

</div>

<!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>