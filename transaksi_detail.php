<?php
	//Koneksi Database
	$server = "localhost";
	$user = "root";
	$pass = "";
	$database = "bengkelmotor";

	// $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));
	$koneksi = mysqli_connect($server, $user, $pass, $database);
		if (!$koneksi) {
			die("<script>alert('Gagal Terhubung ke Database')</script>");
	};
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Customer AlvinMotor</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">
    <div class="container">     
        <h1 class="text-center">Data Riwayat Transaksi AlvinMotor</h1>

        <!-- Awal Card Tabel Detail-->
	    <div class="card mt-3">
            <div class="card-header bg-success text-white">
                Data Riwayat Transaksi AlvinMotor
            </div>
			<div>
				<a href="index.php?hal=inputdata">
					<button type="button" class="btn btn-info"  style="margin: 20px;">Tambah Data</button>
				</a>
				<!-- <a href="cetakfile.php">
					<button type="button" class="btn btn-info"  style="margin: 5px;">Print Data</button>
				</a> -->
			</div>
	  	        <div class="card-body">
	                <div class="table-responsive">
		                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Harga Service</th>
                                    <th>Nama Sparepart</th>
                                    <th>Harga Sparepart</th>
                                    <th>Total Harga</th>
									<th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Harga Service</th>
                                    <th>Nama Sparepart</th>
                                    <th>Harga Sparepart</th>
                                    <th>Total Harga</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                    include 'config.php';
                                    $no = 1;
                                    // SELECT * FROM karyawan, jabatan WHERE karyawan.id_jab = jabatan.id_jab
                                    $data = mysqli_query($conect, "SELECT transaksi_detail.id_tdetail, trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, service.harga_service, 
                                    sparepart.nama_sparepart, sparepart.harga_sparepart, transaksi_detail.total_harga From transaksi_detail INNER JOIN trans_header ON transaksi_detail.id_transaksi = trans_header.id_transaksi 
                                    INNER JOIN service ON transaksi_detail.id_service = service.id_service INNER JOIN sparepart ON transaksi_detail.id_sparepart = sparepart.id_sparepart ORDER BY transaksi_detail.id_tdetail desc");
                                    while ($d = mysqli_fetch_array($data)) {
                                        $d['total_harga'] = $d['harga_service'] + $d['harga_sparepart']
                                ?>
                                        <tr>
                                            <td><?=$no++;?></td>
                                            <td> <?php echo $d['id_transaksi'];?> </td> 
                                            <td> <?php echo $d['tanggal_transaksi'];?> </td>
                                            <td> <?php echo $d['nama_service'];?> </td>
                                            <td> <?php echo $d['harga_service'];?> </td>
                                            <td> <?php echo $d['nama_sparepart'];?> </td>
                                            <td> <?php echo $d['harga_sparepart'];?> </td>
                                            <td> <?php echo $d['total_harga'];?> </td>
											<td>
				    							<a href="cetakfile.php?id_tdetail=<?=$d['id_tdetail']?>" class="btn btn-success"> Print </a>
                                        </tr>

                                    <?php
                                    }        // endwhile; //penutup perulangan while
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <!-- Akhir Card Tabel Detail-->
    </div>
        

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
	    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	    <!-- Core plugin JavaScript-->
	    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	    <!-- Custom scripts for all pages-->
	    <script src="js/sb-admin-2.min.js"></script>

	    <!-- Page level plugins -->
	    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
	    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	    <!-- Page level custom scripts -->
	    <script src="js/demo/datatables-demo.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>

</html>