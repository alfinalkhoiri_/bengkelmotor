<?php
	//Koneksi Database
	$server = "localhost";
	$user = "root";
	$pass = "";
	$database = "bengkelmotor";

	// $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));
	$koneksi = mysqli_connect($server, $user, $pass, $database);
		if (!$koneksi) {
			die("<script>alert('Gagal Terhubung ke Database')</script>");
	};

            //Tampilkan Data yang akan print
            $tampil = mysqli_query($koneksi, "SELECT trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, 
                                    teknisi.nama_teknisi, customer.nama_cs From trans_header INNER JOIN service ON trans_header.id_service = service.id_service 
                                    INNER JOIN teknisi ON trans_header.id_teknisi = teknisi.id_teknisi INNER JOIN customer ON trans_header.id_customer = customer.id_cs 
                                    ORDER BY trans_header.id_transaksi desc");

			$data = mysqli_fetch_array($tampil);
			if($data)
			{
				//Jika data ditemukan, maka data ditampung ke dalam variabel
				// $vidtrans = $data['id_transaksi'];
                $vtanggal = $data['tanggal_transaksi'];
                $vids = $data['nama_service'];
                $vidt = $data['nama_teknisi'];
                $vidc = $data['nama_cs'];
			}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Customer AlvinMotor</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script>
		window.print();
	</script>
</head>

<body id="page-top">
    <div class="container">     
        <h1 class="text-center">Data Transaksi Service AlvinMotor</h1>
        <!-- Data yg akan ditampilakan -->
	    <div class="card mt-3">
            <div class="card-header bg-success text-white" style="margin-bottom: 10px;">
            <table>
                <tr>
                    <td>Tanggal Transaksi</td>
                    <td>:</td>
                    <td><?=@$vtanggal?></td>
                </tr>
                <tr>
                    <td>Nama Customer</td>
                    <td>:</td>
                    <td><?=@$vidc?></td>
                </tr>
                <tr>
                    <td>Nama Teknisi</td>
                    <td>:</td>
                    <td><?=@$vidt?></td>
                </tr>
            </table>
            </div>
	  	        <div class="card-body">
	                <div class="table-responsive">
		                <table border="1" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Service</th>
                                    <th>Harga Service</th>
                                    <th>Nama Sparepart</th>
                                    <th>Harga Sparepart</th>
                                    <th>Total Harga</th>
                                </tr>
                                <!-- <tr>
                                    <th>Total Transaksi</th>
                                </tr>   -->
                            </thead>
                            <tbody>
                                <?php
                                    include 'config.php';
                                    $no = 1;
                                    if(isset($_GET['id_tdetail'])){
                                        $id_tdetail    =$_GET['id_tdetail'];
                                    }
                                    else {
                                        die ("Error. No ID Selected!");    
                                    }
                                    // $t += $d['total_harga'];
                                    $data = mysqli_query($conect, "SELECT transaksi_detail.id_tdetail, trans_header.id_transaksi, trans_header.tanggal_transaksi, service.nama_service, service.harga_service, 
                                    sparepart.nama_sparepart, sparepart.harga_sparepart, transaksi_detail.total_harga From transaksi_detail INNER JOIN trans_header ON transaksi_detail.id_transaksi = trans_header.id_transaksi 
                                    INNER JOIN service ON transaksi_detail.id_service = service.id_service INNER JOIN sparepart ON transaksi_detail.id_sparepart = sparepart.id_sparepart WHERE transaksi_detail.id_tdetail = '$id_tdetail'");
                                    while ($d = mysqli_fetch_array($data)) {
                                        $d['total_harga'] = $d['harga_service'] + $d['harga_sparepart']
                                ?>
                                        <tr>
                                            <td> <?php echo $d['id_transaksi'];?> </td> 
                                            <td> <?php echo $d['tanggal_transaksi'];?> </td>
                                            <td> <?php echo $d['nama_service'];?> </td>
                                            <td> <?php echo $d['harga_service'];?> </td>
                                            <td> <?php echo $d['nama_sparepart'];?> </td>
                                            <td> <?php echo $d['harga_sparepart'];?> </td>
                                            <td> <?php echo $d['total_harga'];?> </td>
                                        </tr>
                                    <?php
                                    }        // endwhile; //penutup perulangan while
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <!-- Akhir Card Tabel Detail-->
    </div>
        

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
	    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	    <!-- Core plugin JavaScript-->
	    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	    <!-- Custom scripts for all pages-->
	    <script src="js/sb-admin-2.min.js"></script>

	    <!-- Page level plugins -->
	    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
	    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

	    <!-- Page level custom scripts -->
	    <script src="js/demo/datatables-demo.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>

</html>