<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Tables</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body>
<div class="container">
    <h1 class="text-center">Input Data Customer</h1>
    <!-- Awal Card Form -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Customer</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form  method="post" action="proses_input.php">
                    <div class="form-group">
                        <label>Nama Customer : </label>
                        <input type="text" name="nama_cs" class="form-control" />
                    </div> 
                    <div class="form-group">
                        <label for="jk1">Jenis Kelamin :</label>
            			<select class="form-control" name="jk_cs">
            				<option>Laki-laki</option>
            				<option>Perempuan</option>
            			</select>
                    </div>
                    <div class="form-group">
                        <label>No Handphone : </label>
                        <input type="text" name="no_hp" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Alamat : </label>
                        <input type="text" name="alamat" class="form-control"/>
                    </div>

                    <button type="submit" name="submit" class="btn btn-primary" style="margin-top: 10px;">Submit</button>
                    <button type="reset" name="reset" class="btn btn-primary" style="margin-top: 10px;">Hapus</button>
                </form>
            </div>        
        </div>
    </div>
    <!-- Akhir Card Form -->
</div>
</body>
</html>