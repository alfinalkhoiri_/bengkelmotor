<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Load file CSS Bootstrap offline -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Nasabah PT. Bank Negara</title>
</head>
<body>
<div class="container">
<h2>Edit Data Nasabah</h2>

<?php
	include 'config.php';
	$id_cs = $_GET['id_cs'];
    $data = mysqli_query($conect, "SELECT * From customer WHERE id_cs='$id_cs'");
    while ($d = mysqli_fetch_array($data)) {
?>
    <form  method="post" action="proses_edit.php">
        <div class="form-group">
            <label>Id Customer : </label>
            <input type="text" name="id_cs" class="form-control" value="<?php echo $d['id_cs'];?>"/>
        </div>
        <div class="form-group">
            <label>Nama Customer : </label>
            <input type="text" name="nama_cs" class="form-control" value="<?php echo $d['nama_cs'];?>"/>
        </div>
        <div class="form-group">
            <label for="jk1">Jenis Kelamin :</label>
			<select class="form-control" name="jk_cs" value="required"/>
                <option value="Laki-laki">Laki-laki</option>
				<option value="Perempuan">Perempuan</option>
			</select>
        </div>
        <div class="form-group">
            <label>No Handphone : </label>
            <input type="text" name="no_hp" class="form-control" value="<?php echo $d['no_hp'];?>"/>
        </div>
        <div class="form-group">
            <label>Alamat : </label>
            <input type="text" name="alamat" class="form-control" value="<?php echo $d['alamat'];?>"/>
        </div>

        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
        <button type="reset" name="reset" value="reset" class="btn btn-primary">Hapus</button>
    </form>
<?php
    }
?>
</div>
</body>
</html>