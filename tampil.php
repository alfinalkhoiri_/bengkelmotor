<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Customer AlvinMotor</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="css/sb-admin-2.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <h1 class="text-center">Data Customer AlvinMotor</h1>
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Customer</h6>
                </div>
                <a href="index.php?hal=inputdata">
                    <button type="button" class="btn btn-info"  style="margin: 20px;">Tambah Data</button></a>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Customer</th>
                                    <th>Nama Customer</th>
                                    <th>Jenis Kelamin</th>
                                    <th>No Handphone</th>
                                    <th>Alamat</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>ID Customer</th>
                                    <th>Nama Customer</th>
                                    <th>Jenis Kelamin</th>
                                    <th>No Handphone</th>
                                    <th>Alamat</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                    include 'config.php';

                                    $no = 1;
                                    $data = mysqli_query($conect, "SELECT * From customer order by id_cs desc");
                                    while ($d = mysqli_fetch_array($data)) :
                                ?>
                                        <tr>
                                            <td><?=$no++;?></td>
                                            <td> <?php echo $d['id_cs'];?> </td> 
                                            <td> <?php echo $d['nama_cs'];?> </td>
                                            <td> <?php echo $d['jk_cs'];?> </td>
                                            <td> <?php echo $d['no_hp'];?> </td>
                                            <td> <?php echo $d['alamat'];?> </td>
                                            <td>
                                                <button type="button" class="btn btn-warning">
                                                <a href="index.php?hal=edit_data&id_cs=<?=$d['id_cs'];?>"> E d i t</a> </button> | 
                                                <button type="button" class="btn btn-danger">
                                                <a href="index.php?hal=delete&id_cs=<?=$d['id_cs'];?>">Delete</button>
                                            </td>
                                        </tr>

                                    <?php
                                    endwhile; //penutup perulangan while
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div> -->

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>